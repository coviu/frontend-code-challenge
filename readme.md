# Front End Developer Code Challenge

Welcome to the Coviu front end developer code challenge. This is just a simple exercise designed to help us get a feel for how you would work with us a team.

## The challenge

Arm and a Leg, Inc are looking to take advantage of Coviu's call sessions API (https://github.com/coviu/coviu-js-sdk) to allow them to schedule calls for their customers.

They've already written a simple Node.js server to handle proxying the requests to the sessions API - all they need now is an interface with which to do it.

The framework they've decided on is Mercury (https://github.com/Raynos/mercury), but could be swayed to use React (https://reactjs.org/).

What they are looking for in the MVP is:
- Be able to create a new session
- Retrieve a session (along with the relevant call links)
- List all the sessions

## Aim of the challenge

The aim of the challenge is for us to get a feel for how it would be to work with you as part of the team, and get an understanding of how you work and approach problems.

This is not a test designed to test your recall of algorithms you learnt 5 years, but can't recall of the top of your head. It's also not a test of whether or not you're familiar with Mercury. You might not finish, but that's also ok - what we want to see are your skills, how you work, and how you tackle problems.

## Your resources

At your disposal you have the following:

### Front end client
Under `/client`, this is the beginnings of a front end application using Mercury, and CSS Modules.

You can get it building using `npm run client`, which will setup a webpack development server which will serve the application on http://localhost:9900

__Potentially useful resources__

- https://github.com/Raynos/mercury

### Back end server

This a prebuilt application ready and able to start proxying requests to the Coviu API. It talks to the Coviu staging server (https://covi.io) and is available on http://localhost:9901 after running `npm run server`.

It has the following routes (these correspond to functions exposed via the Coviu JS SDK - see https://github.com/coviu/coviu-js-sdk):

`GET /sessions`
Returns a list of sessions.

Query string options:
{
    "page": optional number,
    "page_size": optional number,
    "start_time": optional utc date string,
    "end_time": optional utc date string
    "include_canceled": optional boolean
}

`POST /session`
Creates a new session.

Body parameters:
{
    "session_name": optional display name for the session
    "start_time": utc date string,
    "end_time": utc date string,
    "picture": optional url for room image,
    "participants": [{
    "display_name": optional string for participant display name,
    "picture": option url for participant avatar,
    "role": required - "guest", or "host",
    "state": option content for client use
    }, ...]
}

`GET /session/:sessionId`
Returns the session indicated by the `sessionId` parameter

`PUT /session/:sessionId`
Updates the session indicated by the `sessionId` parameter

Body parameters:
{
    "session_name": optional display name for the session
    "start_time": optional utc date string,
    "end_time": optional utc date string,
    "picture": optional utc url for room image,
}

`DELETE /session:sessionId`
Cancels the session indicated by the `sessionId` parameter


__Potentially useful resources__

- https://github.com/coviu/coviu-js-sdk

- https://github.com/coviu/coviu-sdk-api/blob/master/libs/sessions.js (session API source)