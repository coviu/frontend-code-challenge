const webpack = require('webpack');
const path = require('path');

module.exports = {
	entry: {
		app: './client/index.js'
	},
	output: {
		path: __dirname + '/dist',
		filename: '[name].js'
	},
	module: {
		loaders: [{
			/* Only run Babel on our local JS, not our dependencies */
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader',
		},
        // Add support for JSON files
		{ test: /\.json$/, loader: 'json' },
        // Add support for SCSS modules
		{
			test: /\.scss$/,
			loader: 'style!css?modules&module&importLoaders=2&sourceMap&localIdentName=[local]___[hash:base64:5]!sass?sourceMap'
		}]
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		}),
		new webpack.optimize.DedupePlugin()
	],
	devtool: 'hidden-source-map',
	node: {
		console: true,
		net: 'empty',
		tls: 'empty'
	},
	devServer: {
		historyApiFallback: {
			index: '/index.html'
		},
		stats: {
			info: false
		}
	}
};
