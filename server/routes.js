const sdk = require('coviu-js-sdk');
const express = require('express');
const router = express.Router();

// Configure the SDK
const apiKey = undefined;
const apiSecret = undefined;
if (!apiKey || !apiSecret) throw new Error('No API key or secret provided');
const coviu = sdk(apiKey, apiSecret, {
    endpoint: 'https://api-staging.covi.io/v1'
});

const proxy = (fn) => {
    if (!fn) throw new Error('Invalid method to proxy');
    return (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        return fn(req).run().then((r) => {
            return res.status(200).json(r);
        }).catch((err) => {
            return res.status(500).json({ error: err.message || err });
        })
    }
};

router.get('/sessions', proxy((req) => coviu.sessions.getSessions(req.query || {})))
router.post('/session', proxy((req) => coviu.sessions.createSession(req.body)))
router.get('/session/:sessionId', proxy((req) => coviu.sessions.getSession(req.params.sessionId)))
router.put('/session/:sessionId', proxy((req) => coviu.sessions.updateSession(req.params.sessionId, req.body)));
router.delete('/session/:sessionId', proxy((req) => coviu.sessions.cancelSession(req.params.sessionId)));

module.exports = router;