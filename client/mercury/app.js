import mercury from 'mercury'
import renderer from './renderer'
import state from './state'

mercury.app(document.getElementById('app'), state, renderer);