import { h } from 'mercury'
import styles from '../styles/app.scss'

export default function (state) {
    return h(`div.${styles.App}`, 'A wild app appeared')
}