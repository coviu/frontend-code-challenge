import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styles from '../styles/app.scss'

class App extends Component {
    render() {
        return (<div className={styles.App}>A wild app appeared</div>);
    }
}

ReactDOM.render(<App />, document.getElementById('app'));